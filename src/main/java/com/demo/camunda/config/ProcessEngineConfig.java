package com.demo.camunda.config;

import org.camunda.bpm.engine.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.inject.Inject;

@Configuration
public class ProcessEngineConfig {

    @Inject
    ProcessEngine processEngine;

    @Primary
    @Bean("camundaEngine")
    public ProcessEngine camundaEngine(){
        return processEngine;
    }

    @Bean
    public RepositoryService repositoryService(ProcessEngine camundaEngine) {
        return camundaEngine.getRepositoryService();
    }

    @Bean
    public RuntimeService runtimeService(ProcessEngine camundaEngine) {
        return camundaEngine.getRuntimeService();
    }

    @Bean
    public TaskService taskService(ProcessEngine camundaEngine) {
        return camundaEngine.getTaskService();
    }

    @Bean
    public HistoryService historyService(ProcessEngine camundaEngine) {
        return camundaEngine.getHistoryService();
    }

    @Bean
    public ManagementService managementService(ProcessEngine camundaEngine) {
        return camundaEngine.getManagementService();
    }

}
